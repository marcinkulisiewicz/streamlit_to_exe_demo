# streamlit_to_exe_demo

Streamlit demo web app with an instruction how to deploy it as a stand-alone application on client machine (Windows platforms).

## Getting started

Instructions are available here:
https://infosystechnologies-my.sharepoint.com/personal/marcin_kulisiewicz_ad_infosys_com/Documents/HOWTO-Streamlit_to_exe.pdf
