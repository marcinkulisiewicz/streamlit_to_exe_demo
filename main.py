import streamlit as st
import requests


DECK_SHUFFLE_ENDPOINT = "https://deckofcardsapi.com/api/deck/new/shuffle/?deck_count=1"
NR_OF_CARDS = 5
deck_id = None


def get_poker_hand():
	print('getting new deck')
	response = requests.get(DECK_SHUFFLE_ENDPOINT)
	if response.status_code == 200:
		print('drawing cards')
		deck_id = response.json()['deck_id']
		cards_images = []
		with st.spinner('Drawing cards...'):
			for i in range(NR_OF_CARDS):
				cards_response = requests.get(f'https://deckofcardsapi.com/api/deck/{deck_id}/draw/?count/1')
				if cards_response.status_code == 200:
					card = cards_response.json()['cards'][0]
					print(f"CARD: {card['value']} of {card['suit']}")
					cards_images.append(card['image'])
				else:
					st.warning('API error - draw request')
		st.image(cards_images , width=100)
	else:
		st.warning('API error - initial request')


if __name__ == '__main__':
	st.header("Demo app - poker hand")
	draw_button = st.button("new hand")
	if draw_button:
		get_poker_hand()
